/**
 * jux.wtf - Consolidate, maintain, share and search your nomenclature.
 * Copyright (C) 2021 Daniel Holm
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Template } from 'meteor/templating'
import { FlowRouter } from 'meteor/ostrio:flow-router-extra'
import { Dictionary } from '../imports/dictionary'
import { Word } from '../imports/word'

Template.dictionary.onCreated(function helloOnCreated() {
  const self = this
  self.autorun(() => {
    self.subscribe('dictionary', FlowRouter.getParam('_dictionaryIdentifier'))
    self.subscribe('words', FlowRouter.getParam('_dictionaryIdentifier'))
  })
})

Template.dictionary.helpers({
  dictionary: function() {
    return Dictionary.findOne({ identifier: FlowRouter.getParam('_dictionaryIdentifier') })
  },
  word: function() {
    return Word.find({ dictionary: FlowRouter.getParam('_dictionaryIdentifier') })
  }
})