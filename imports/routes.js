/**
 * jux.wtf - Consolidate, maintain, share and search your nomenclature.
 * Copyright (C) 2021 Daniel Holm
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { FlowRouter } from 'meteor/ostrio:flow-router-extra'
import { BlazeLayout } from 'meteor/kadira:blaze-layout'

FlowRouter.route('/', {
  name: 'Front',
  action: function(_params, _queryParams) {
    BlazeLayout.render('layout', { main: 'front' })
  }
})

FlowRouter.route('/dictionaries', {
  name: 'Dictionaries',
  action: function(_params, _queryParams) {
    BlazeLayout.render('layout', { main: 'dictionaries' })
  }
})

FlowRouter.route('/dictionary', {
  name: 'Dictionary',
  triggersEnter: [function(_context, redirect) {
    redirect('/dictionaries')
  }]
})

FlowRouter.route('/dictionary/:_dictionaryIdentifier', {
  name: 'Dictionary',
  action: function(_params, _queryParams) {
    BlazeLayout.render('layout', { main: 'dictionary' })
  }
})

FlowRouter.route('/dictionary/:_dictionaryIdentifier/:_wordIdentifier', {
  name: 'Dictionary.detail',
  action: function(_params, _queryParams) {
    BlazeLayout.render('layout', { main: 'word' })
  }
})

FlowRouter.route('*', {
  action() {
    this.render('not_found');
  }
})