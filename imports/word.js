/**
 * jux.wtf - Consolidate, maintain, share and search your nomenclature.
 * Copyright (C) 2021 Daniel Holm
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Mongo } from 'meteor/mongo'

export const Word = new Mongo.Collection('word')

if (Meteor.isServer) {
  Meteor.publish('words', function wordsPublication(dictionary) {
    return Word.find({ dictionary: dictionary })
  })

  Meteor.publish('word', function wordPublication(dictionary, identifier) {
    return Word.find({ dictionary: dictionary, identifier: identifier })
  })
}
