/**
 * jux.wtf - Consolidate, maintain, share and search your nomenclature.
 * Copyright (C) 2021 Daniel Holm
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Meteor } from 'meteor/meteor'
import '../imports/dictionary'
import { Dictionary } from '../imports/dictionary'
import { Word } from '../imports/word'

Meteor.startup(() => {
  // code to run on server at startup
  const now = new Date()

  if (Word.find().count() <= 0) {
    Dictionary.insert({
      identifier: 'jux-standard',
      created: now,
      modified: now
    })
  }

  if (Dictionary.find().count() <= 0) {
    Word.insert({ 
      dictionary: 'jux-standard',
      identifier: 'farce-development', 
      word: 'Farce Development', 
      description: 'When the entire software development project is a complete farce. The state of farce is achieved when the project lacks use of any established or sensible processes.',
      created: now,
      modified: now 
    })
  }
})
